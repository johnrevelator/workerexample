package ru.alexanderklimov.workerexample.di.component

import dagger.Component
import ru.alexanderklimov.workerexample.App
import ru.alexanderklimov.workerexample.di.module.AppApiModule
import ru.alexanderklimov.workerexample.di.module.ApplicationModule
import ru.alexanderklimov.workerexample.di.module.ViewModelModule
import ru.alexanderklimov.workerexample.presentation.ui.activity.MainActivity
import ru.alexanderklimov.workerexample.presentation.worker.DownloadFileWorker
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, AppApiModule::class, ViewModelModule::class])
interface ApplicationComponent {
    fun inject(target: App)
    fun inject(target: MainActivity)
    fun inject(target: DownloadFileWorker)
}

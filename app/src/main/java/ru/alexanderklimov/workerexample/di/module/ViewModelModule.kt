package ru.alexanderklimov.workerexample.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.alexanderklimov.workerexample.di.ViewModelKey
import ru.alexanderklimov.workerexample.presentation.ui.viewmodels.MainViewModel
import ru.alexanderklimov.workerexample.presentation.ui.viewmodels.ViewModelFactory

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun mainViewModel(viewModel: MainViewModel): ViewModel
}

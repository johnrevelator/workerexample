package ru.alexanderklimov.workerexample.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideApp() = application

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = application.applicationContext

}

package ru.alexanderklimov.workerexample.presentation.ui.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.work.WorkInfo
import ru.alexanderklimov.workeexample.R
import ru.alexanderklimov.workeexample.databinding.ActivityMainBinding
import ru.alexanderklimov.workerexample.App
import ru.alexanderklimov.workerexample.presentation.extentions.injectViewModel
import ru.alexanderklimov.workerexample.presentation.ui.viewmodels.MainViewModel
import ru.alexanderklimov.workerexample.presentation.worker.DownloadFileWorker
import javax.inject.Inject


class MainActivity : BaseActivity() {

    companion object {
        const val REQUEST_CODE = 1
        const val FILE_NAME = "100MB.bin"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: MainViewModel

    private val binding by binding<ActivityMainBinding>(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component.inject(this)
        viewModel = injectViewModel(viewModelFactory)

        binding.apply {
            lifecycleOwner = this@MainActivity
        }

        binding.downloadFile.setOnClickListener {
            if (!needSetupPermissions())
                downloadFile()
        }

        viewModel.downloadProgress.observe(::getLifecycle) { list ->
            list.firstOrNull { it.state == WorkInfo.State.RUNNING }?.let {
                val downloadProgress = it.progress.getInt(DownloadFileWorker.PROGRESS, -1)
                binding.downloadProgress.apply {
                    progress = downloadProgress
                    isVisible = true
                }
                binding.downloadProgressValue.apply {
                    if (downloadProgress > 0)
                        text = "$downloadProgress%"
                    isVisible = true
                }
            } ?: run {
                binding.downloadProgress.isVisible = false
                binding.downloadProgressValue.isVisible = false
            }
        }
    }

    private fun downloadFile() {
        viewModel.downloadFile(FILE_NAME)
    }

    private fun needSetupPermissions(): Boolean {
        return if (
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            makeRequest()
            true
        } else false
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_CODE && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            downloadFile()
        }
    }
}
package ru.alexanderklimov.workerexample.presentation.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext
import ru.alexanderklimov.workerexample.App
import ru.alexanderklimov.workerexample.data.dto.DownloadResult
import ru.alexanderklimov.workerexample.data.repositories.DownloadRepository
import ru.alexanderklimov.workerexample.presentation.extentions.saveFile
import java.io.File
import javax.inject.Inject

class DownloadFileWorker(context: Context, workerParams: WorkerParameters) :
    CoroutineWorker(context, workerParams) {

    init {
        App.component.inject(this)
    }

    @Inject
    lateinit var downloadRepository: DownloadRepository

    override suspend fun doWork(): Result {
        return withContext(Dispatchers.IO) {
            try {
                inputData.getString(DOWNLOADING_FILE)?.let { file ->
                    downloadRepository.downloadFile(file).saveFile(File(inputData.getString(FILE)))
                        .collect {
                            when (it) {
                                is DownloadResult.Success -> {
                                    Result.success()
                                }
                                is DownloadResult.Error -> {
                                    Result.failure()
                                }
                                is DownloadResult.Progress -> {
                                    setProgress(
                                        Data.Builder().putInt(PROGRESS, it.progress).build()
                                    )
                                }
                            }
                        }
                }
                Result.success()
            } catch (e: Exception) {
                Log.d(TAG, e.message.toString())
                Result.failure()
            }
        }
    }

    companion object {
        const val TAG = "DownloadFile"
        const val PROGRESS = "progress"
        const val FILE = "file"
        const val DOWNLOADING_FILE = "downloading_file"
    }
}
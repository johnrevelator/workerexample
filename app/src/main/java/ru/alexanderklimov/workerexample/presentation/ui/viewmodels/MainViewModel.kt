package ru.alexanderklimov.workerexample.presentation.ui.viewmodels

import android.app.Application
import android.os.Environment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import ru.alexanderklimov.workerexample.App
import ru.alexanderklimov.workerexample.presentation.worker.DownloadFileWorker
import ru.alexanderklimov.workerexample.presentation.worker.DownloadFileWorker.Companion.DOWNLOADING_FILE
import ru.alexanderklimov.workerexample.presentation.worker.DownloadFileWorker.Companion.FILE
import java.io.File
import javax.inject.Inject


class MainViewModel @Inject constructor(application: Application) : AndroidViewModel(application) {

    var downloadProgress: LiveData<List<WorkInfo>> = WorkManager.getInstance(getApplication())
        .getWorkInfosByTagLiveData(DownloadFileWorker::class.java.simpleName)

    var workManager = WorkManager.getInstance(application)

    fun downloadFile(file: String) {
        val request = OneTimeWorkRequestBuilder<DownloadFileWorker>()
            .addTag(DownloadFileWorker::class.java.simpleName)
            .setInputData(Data.Builder().apply {
                putString(FILE, createFile(file).absolutePath)
                putString(DOWNLOADING_FILE, file)
            }.build())
            .build()
        workManager.enqueue(request)
    }

    private fun createFile(name: String): File {
        val mapsFolder = File(
            getApplication<App>().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS),
            "/$name"
        )
        if (mapsFolder.exists())
            mapsFolder.delete()
        mapsFolder.createNewFile()
        return mapsFolder
    }
}
package ru.alexanderklimov.workerexample.presentation.extentions

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody
import ru.alexanderklimov.workerexample.data.dto.DownloadResult
import java.io.File

suspend fun ResponseBody.saveFile(file: File): Flow<DownloadResult> {
    return flow {
        emit(DownloadResult.Progress(0))
        var deleteFile = true
        try {
            byteStream().use { inputStream ->
                file.outputStream().use { outputStream ->
                    val totalBytes = contentLength()
                    val data = ByteArray(8_192)
                    var progressBytes = 0L

                    while (true) {
                        val bytes = inputStream.read(data)

                        if (bytes == -1) {
                            break
                        }

                        outputStream.channel
                        outputStream.write(data, 0, bytes)
                        progressBytes += bytes
                        val percent = (progressBytes.toFloat() / totalBytes.toFloat()) * 100
                        emit(DownloadResult.Progress(percent.toInt()))
                    }

                    when {
                        progressBytes < totalBytes ->
                            throw Exception("missing bytes")
                        progressBytes > totalBytes ->
                            throw Exception("too many bytes")
                        else ->
                            deleteFile = false
                    }
                }
            }
            emit(DownloadResult.Success)
        } catch (e: Exception) {
            emit(DownloadResult.Error("File not downloaded"))
        } finally {
            if (deleteFile) {
                file.delete()
            }
        }
    }
}
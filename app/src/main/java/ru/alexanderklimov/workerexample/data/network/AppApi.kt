package ru.alexanderklimov.workerexample.data.network

import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Streaming

class AppApi {

    companion object {
        const val BASE_URL = "https://speed.hetzner.de/"
    }

    interface Files {

        @Streaming
        @GET("{fileName}")
        suspend fun getFile(@Path("fileName") fileName: String): ResponseBody
    }
}

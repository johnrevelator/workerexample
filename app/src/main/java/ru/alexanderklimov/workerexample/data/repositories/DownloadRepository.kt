package ru.alexanderklimov.workerexample.data.repositories

import ru.alexanderklimov.workerexample.data.network.AppApi
import javax.inject.Inject

class DownloadRepository @Inject constructor(
    private val downloadApi: AppApi.Files
) {
    suspend fun downloadFile(mapUrl: String) = downloadApi.getFile(mapUrl)
}
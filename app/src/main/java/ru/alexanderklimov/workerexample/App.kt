package ru.alexanderklimov.workerexample

import android.app.Application
import ru.alexanderklimov.workerexample.di.component.ApplicationComponent
import ru.alexanderklimov.workerexample.di.component.DaggerApplicationComponent
import ru.alexanderklimov.workerexample.di.module.AppApiModule
import ru.alexanderklimov.workerexample.di.module.ApplicationModule

class App : Application() {

    companion object {
        lateinit var component: ApplicationComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        initDI()
        component.inject(this)
    }

    private fun initDI() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .appApiModule(AppApiModule(this))
            .build()
    }
}